package com.example.akshay.sqlitewithmysql;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;

/**
 * Created by akshay on 06/11/17.
 */

public class RecylerViewAdapter extends RecyclerView.Adapter<RecylerViewAdapter.MyViewHOlder> {

    Context context,context1;
    public ArrayList<data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX> arrayList = new ArrayList<>();

    Cursor cursor;
    boolean isConnected;


    public RecylerViewAdapter(ArrayList<data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

    }

    @Override
    public MyViewHOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recylerview_each_item_layout,parent,false);
        return new MyViewHOlder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHOlder holder, int position) {

        final data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX postBean = arrayList.get(position);
        ConnectivityManager cm =(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        Log.d("avtar",postBean.getAvatar());
        Log.d("detail",postBean.getDetail());

            Picasso.with(context).load(postBean.getAvatar()).into(holder.dp);

            //we are using StringEscapeUtils.unescapeJava or emoji decoding from unicode => emoji
            holder.post.setText("hii there " + StringEscapeUtils.unescapeJava(postBean.getDetail()));


    }

    @Override
    public int getItemCount() {

                 return arrayList.size();
    }

    public class MyViewHOlder extends RecyclerView.ViewHolder {

        ImageView dp;
        TextView post;
        public MyViewHOlder(View itemView) {
            super(itemView);
            dp=(ImageView)itemView.findViewById(R.id.item_imageView);
            post=(TextView)itemView.findViewById(R.id.item_textView);
        }
    }
}
