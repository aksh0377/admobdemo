package com.example.akshay.sqlitewithmysql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Belal on 1/27/2017.
 */
public class DbHelper extends SQLiteOpenHelper {

    //Constants for Database name, table name, and column names
    public static final String DB_NAME = "NamesDB";
    public static final String TABLE_NAME = "names";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PROFILE = "profile";
    public static final String COLUMN_STATUS = "status";

    //database version
    private static final int DB_VERSION = 2;

    //Constructor
    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //creating the database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" ("+COLUMN_ID+"  INTEGER PRIMARY KEY, "+COLUMN_NAME+" VARCHAR, "+COLUMN_PROFILE+"  VARCHAR "+")";
        db.execSQL(CREATE_TABLE);
    }

    //upgrading the database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS names";
        db.execSQL(sql);
        onCreate(db);
    }

}