package com.example.akshay.sqlitewithmysql;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by akshay on 06/11/17.
 */

public class data_pojo {


    private List<DataBeanXXXXXXX> data;

    public List<DataBeanXXXXXXX> getData() {
        return data;
    }

    public void setData(List<DataBeanXXXXXXX> data) {
        this.data = data;
    }

    public static class DataBeanXXXXXXX {
        /**
         * SocialAccount : {"data":[{"SocialAccount":{"data":{"identity":"rpPDp","social_media":"facebook","is_company":1,"site_identity":0}}}]}
         * post : {"data":{"identity":"Opjmp","type":"external","detail":"This is my #firstpost on fb!","approved":"Approved","post_status":"Draft","like_count":2,"share_count":0,"comment_count":25,"latitude":22.9365808,"longitude":72.4575238,"created":{"date":"2017-10-26 09:36:46.000000","timezone_type":3,"timezone":"UTC"},"is_liked":true,"postOwner":{"data":{"identity":"Opjmp","firstname":"james","lastname":"sparrow","email":"vipul@pirates.com","about":"","avatar":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","user_position":"Dev","notification_on":true,"connect_to_social":true,"device":"android","device_id":"APA91bFizURfpzgiXqmLntGcNzuqIjF5WN9hd-WRYM-YVoYykcftzYidQ7SV5rBxxu-lMIU44W3iU76u1O13CFJ0Zcsox9VQv_jbzAew1MeVssF6nFWeXyB6LKzUpWdwkvY2GJoy1IGe","lat":0,"long":0,"auth_token":"Ic7E3N","paused":0,"Department":{"data":{"identity":"Opjmp","name":"IT","total_user":1,"total_post":77}}}},"assetDetail":{"data":[]},"tags":{"data":[{"identity":"rpPDp","name":"firstpost","tag_type":"post"}]},"userTag":{"data":[{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"},{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"}]}}}
         */

        private SocialAccountBeanX SocialAccount;
        private PostBean post;

        public SocialAccountBeanX getSocialAccount() {
            return SocialAccount;
        }

        public void setSocialAccount(SocialAccountBeanX SocialAccount) {
            this.SocialAccount = SocialAccount;
        }

        public PostBean getPost() {
            return post;
        }

        public void setPost(PostBean post) {
            this.post = post;
        }

        public static class SocialAccountBeanX {
            private List<DataBeanX> data;

            public List<DataBeanX> getData() {
                return data;
            }

            public void setData(List<DataBeanX> data) {
                this.data = data;
            }

            public static class DataBeanX {
                /**
                 * SocialAccount : {"data":{"identity":"rpPDp","social_media":"facebook","is_company":1,"site_identity":0}}
                 */

                private SocialAccountBean SocialAccount;

                public SocialAccountBean getSocialAccount() {
                    return SocialAccount;
                }

                public void setSocialAccount(SocialAccountBean SocialAccount) {
                    this.SocialAccount = SocialAccount;
                }

                public static class SocialAccountBean {
                    /**
                     * data : {"identity":"rpPDp","social_media":"facebook","is_company":1,"site_identity":0}
                     */

                    private DataBean data;

                    public DataBean getData() {
                        return data;
                    }

                    public void setData(DataBean data) {
                        this.data = data;
                    }

                    public static class DataBean {
                        /**
                         * identity : rpPDp
                         * social_media : facebook
                         * is_company : 1
                         * site_identity : 0
                         */

                        private String identity;
                        private String social_media;
                        private int is_company;
                        private int site_identity;

                        public String getIdentity() {
                            return identity;
                        }

                        public void setIdentity(String identity) {
                            this.identity = identity;
                        }

                        public String getSocial_media() {
                            return social_media;
                        }

                        public void setSocial_media(String social_media) {
                            this.social_media = social_media;
                        }

                        public int getIs_company() {
                            return is_company;
                        }

                        public void setIs_company(int is_company) {
                            this.is_company = is_company;
                        }

                        public int getSite_identity() {
                            return site_identity;
                        }

                        public void setSite_identity(int site_identity) {
                            this.site_identity = site_identity;
                        }
                    }
                }
            }
        }

        public static class PostBean {
            /**
             * data : {"identity":"Opjmp","type":"external","detail":"This is my #firstpost on fb!","approved":"Approved","post_status":"Draft","like_count":2,"share_count":0,"comment_count":25,"latitude":22.9365808,"longitude":72.4575238,"created":{"date":"2017-10-26 09:36:46.000000","timezone_type":3,"timezone":"UTC"},"is_liked":true,"postOwner":{"data":{"identity":"Opjmp","firstname":"james","lastname":"sparrow","email":"vipul@pirates.com","about":"","avatar":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","user_position":"Dev","notification_on":true,"connect_to_social":true,"device":"android","device_id":"APA91bFizURfpzgiXqmLntGcNzuqIjF5WN9hd-WRYM-YVoYykcftzYidQ7SV5rBxxu-lMIU44W3iU76u1O13CFJ0Zcsox9VQv_jbzAew1MeVssF6nFWeXyB6LKzUpWdwkvY2GJoy1IGe","lat":0,"long":0,"auth_token":"Ic7E3N","paused":0,"Department":{"data":{"identity":"Opjmp","name":"IT","total_user":1,"total_post":77}}}},"assetDetail":{"data":[]},"tags":{"data":[{"identity":"rpPDp","name":"firstpost","tag_type":"post"}]},"userTag":{"data":[{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"},{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"}]}}
             */

            private DataBeanXXXXXX data;


            public PostBean(String identity, String type, String detail, String approved) {
            }


            public DataBeanXXXXXX getData() {
                return data;
            }

            public void setData(DataBeanXXXXXX data) {
                this.data = data;
            }

            public static class DataBeanXXXXXX {
                /**
                 * identity : Opjmp
                 * type : external
                 * detail : This is my #firstpost on fb!
                 * approved : Approved
                 * post_status : Draft
                 * like_count : 2
                 * share_count : 0
                 * comment_count : 25
                 * latitude : 22.9365808
                 * longitude : 72.4575238
                 * created : {"date":"2017-10-26 09:36:46.000000","timezone_type":3,"timezone":"UTC"}
                 * is_liked : true
                 * postOwner : {"data":{"identity":"Opjmp","firstname":"james","lastname":"sparrow","email":"vipul@pirates.com","about":"","avatar":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","user_position":"Dev","notification_on":true,"connect_to_social":true,"device":"android","device_id":"APA91bFizURfpzgiXqmLntGcNzuqIjF5WN9hd-WRYM-YVoYykcftzYidQ7SV5rBxxu-lMIU44W3iU76u1O13CFJ0Zcsox9VQv_jbzAew1MeVssF6nFWeXyB6LKzUpWdwkvY2GJoy1IGe","lat":0,"long":0,"auth_token":"Ic7E3N","paused":0,"Department":{"data":{"identity":"Opjmp","name":"IT","total_user":1,"total_post":77}}}}
                 * assetDetail : {"data":[]}
                 * tags : {"data":[{"identity":"rpPDp","name":"firstpost","tag_type":"post"}]}
                 * userTag : {"data":[{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"},{"identity":"rpPDp","name":null,"first_name":"jack","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"FRZiaBnfeo","position":"Developer","department":"HR"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"},{"identity":"Opjmp","name":null,"first_name":"james","last_name":"sparrow","type":"user","profile_image":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","auth_token":"Ic7E3N","position":"Dev","department":"IT"}]}
                 */

                private String identity;
                private String type;
                private String detail;
                private String approved;
                private String post_status;
                private int like_count;
                private int share_count;
                private int comment_count;
                private double latitude;
                private double longitude;
                private CreatedBean created;
                private boolean is_liked;
                private PostOwnerBean postOwner;
                private AssetDetailBean assetDetail;
                private TagsBean tags;
                private UserTagBean userTag;
                private String avatar;

                public DataBeanXXXXXX(String identity, String type, String detail, String approved, String avatar) {
                    this.identity =identity;
                    this.type= type;
                    this.detail =detail;
                    this.approved=approved;
                    this.avatar=avatar;
                }




                public String getIdentity() {
                    return identity;
                }

                public void setIdentity(String identity) {
                    this.identity = identity;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getDetail() {
                    return detail;
                }

                public void setDetail(String detail) {
                    this.detail = detail;
                }

                public String getApproved() {
                    return approved;
                }

                public void setApproved(String approved) {
                    this.approved = approved;
                }

                //

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }
                //

                public String getPost_status() {
                    return post_status;
                }

                public void setPost_status(String post_status) {
                    this.post_status = post_status;
                }

                public int getLike_count() {
                    return like_count;
                }

                public void setLike_count(int like_count) {
                    this.like_count = like_count;
                }

                public int getShare_count() {
                    return share_count;
                }

                public void setShare_count(int share_count) {
                    this.share_count = share_count;
                }

                public int getComment_count() {
                    return comment_count;
                }

                public void setComment_count(int comment_count) {
                    this.comment_count = comment_count;
                }

                public double getLatitude() {
                    return latitude;
                }

                public void setLatitude(double latitude) {
                    this.latitude = latitude;
                }

                public double getLongitude() {
                    return longitude;
                }

                public void setLongitude(double longitude) {
                    this.longitude = longitude;
                }

                public CreatedBean getCreated() {
                    return created;
                }

                public void setCreated(CreatedBean created) {
                    this.created = created;
                }

                public boolean isIs_liked() {
                    return is_liked;
                }

                public void setIs_liked(boolean is_liked) {
                    this.is_liked = is_liked;
                }

                public PostOwnerBean getPostOwner() {
                    return postOwner;
                }

                public void setPostOwner(PostOwnerBean postOwner) {
                    this.postOwner = postOwner;
                }

                public AssetDetailBean getAssetDetail() {
                    return assetDetail;
                }

                public void setAssetDetail(AssetDetailBean assetDetail) {
                    this.assetDetail = assetDetail;
                }

                public TagsBean getTags() {
                    return tags;
                }

                public void setTags(TagsBean tags) {
                    this.tags = tags;
                }

                public UserTagBean getUserTag() {
                    return userTag;
                }

                public void setUserTag(UserTagBean userTag) {
                    this.userTag = userTag;
                }

                public static class CreatedBean {
                    /**
                     * date : 2017-10-26 09:36:46.000000
                     * timezone_type : 3
                     * timezone : UTC
                     */

                    private String date;
                    private int timezone_type;
                    private String timezone;

                    public String getDate() {
                        return date;
                    }

                    public void setDate(String date) {
                        this.date = date;
                    }

                    public int getTimezone_type() {
                        return timezone_type;
                    }

                    public void setTimezone_type(int timezone_type) {
                        this.timezone_type = timezone_type;
                    }

                    public String getTimezone() {
                        return timezone;
                    }

                    public void setTimezone(String timezone) {
                        this.timezone = timezone;
                    }
                }

                public static class PostOwnerBean {
                    /**
                     * data : {"identity":"Opjmp","firstname":"james","lastname":"sparrow","email":"vipul@pirates.com","about":"","avatar":"https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png","user_position":"Dev","notification_on":true,"connect_to_social":true,"device":"android","device_id":"APA91bFizURfpzgiXqmLntGcNzuqIjF5WN9hd-WRYM-YVoYykcftzYidQ7SV5rBxxu-lMIU44W3iU76u1O13CFJ0Zcsox9VQv_jbzAew1MeVssF6nFWeXyB6LKzUpWdwkvY2GJoy1IGe","lat":0,"long":0,"auth_token":"Ic7E3N","paused":0,"Department":{"data":{"identity":"Opjmp","name":"IT","total_user":1,"total_post":77}}}
                     */

                    private DataBeanXXX data;

                    public DataBeanXXX getData() {
                        return data;
                    }

                    public void setData(DataBeanXXX data) {
                        this.data = data;
                    }

                    public static class DataBeanXXX {
                        /**
                         * identity : Opjmp
                         * firstname : james
                         * lastname : sparrow
                         * email : vipul@pirates.com
                         * about :
                         * avatar : https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png
                         * user_position : Dev
                         * notification_on : true
                         * connect_to_social : true
                         * device : android
                         * device_id : APA91bFizURfpzgiXqmLntGcNzuqIjF5WN9hd-WRYM-YVoYykcftzYidQ7SV5rBxxu-lMIU44W3iU76u1O13CFJ0Zcsox9VQv_jbzAew1MeVssF6nFWeXyB6LKzUpWdwkvY2GJoy1IGe
                         * lat : 0
                         * long : 0
                         * auth_token : Ic7E3N
                         * paused : 0
                         * Department : {"data":{"identity":"Opjmp","name":"IT","total_user":1,"total_post":77}}
                         */

                        private String identity;
                        private String firstname;
                        private String lastname;
                        private String email;
                        private String about;
                        private String avatar;
                        private String user_position;
                        private boolean notification_on;
                        private boolean connect_to_social;
                        private String device;
                        private String device_id;
                        private int lat;
                        //@com.google.gson.annotations.SerializedName("long")
                        private int longX;
                        private String auth_token;
                        private int paused;
                        private DepartmentBean Department;

                        public DataBeanXXX(String avatar) {
                            this.avatar=avatar;
                        }

                        public String getIdentity() {
                            return identity;
                        }

                        public void setIdentity(String identity) {
                            this.identity = identity;
                        }

                        public String getFirstname() {
                            return firstname;
                        }

                        public void setFirstname(String firstname) {
                            this.firstname = firstname;
                        }

                        public String getLastname() {
                            return lastname;
                        }

                        public void setLastname(String lastname) {
                            this.lastname = lastname;
                        }

                        public String getEmail() {
                            return email;
                        }

                        public void setEmail(String email) {
                            this.email = email;
                        }

                        public String getAbout() {
                            return about;
                        }

                        public void setAbout(String about) {
                            this.about = about;
                        }

                        public String getAvatar() {
                            return avatar;
                        }

                        public void setAvatar(String avatar) {
                            this.avatar = avatar;
                        }

                        public String getUser_position() {
                            return user_position;
                        }

                        public void setUser_position(String user_position) {
                            this.user_position = user_position;
                        }

                        public boolean isNotification_on() {
                            return notification_on;
                        }

                        public void setNotification_on(boolean notification_on) {
                            this.notification_on = notification_on;
                        }

                        public boolean isConnect_to_social() {
                            return connect_to_social;
                        }

                        public void setConnect_to_social(boolean connect_to_social) {
                            this.connect_to_social = connect_to_social;
                        }

                        public String getDevice() {
                            return device;
                        }

                        public void setDevice(String device) {
                            this.device = device;
                        }

                        public String getDevice_id() {
                            return device_id;
                        }

                        public void setDevice_id(String device_id) {
                            this.device_id = device_id;
                        }

                        public int getLat() {
                            return lat;
                        }

                        public void setLat(int lat) {
                            this.lat = lat;
                        }

                        public int getLongX() {
                            return longX;
                        }

                        public void setLongX(int longX) {
                            this.longX = longX;
                        }

                        public String getAuth_token() {
                            return auth_token;
                        }

                        public void setAuth_token(String auth_token) {
                            this.auth_token = auth_token;
                        }

                        public int getPaused() {
                            return paused;
                        }

                        public void setPaused(int paused) {
                            this.paused = paused;
                        }

                        public DepartmentBean getDepartment() {
                            return Department;
                        }

                        public void setDepartment(DepartmentBean Department) {
                            this.Department = Department;
                        }

                        public static class DepartmentBean {
                            /**
                             * data : {"identity":"Opjmp","name":"IT","total_user":1,"total_post":77}
                             */

                            private DataBeanXX data;

                            public DataBeanXX getData() {
                                return data;
                            }

                            public void setData(DataBeanXX data) {
                                this.data = data;
                            }

                            public static class DataBeanXX {
                                /**
                                 * identity : Opjmp
                                 * name : IT
                                 * total_user : 1
                                 * total_post : 77
                                 */

                                private String identity;
                                private String name;
                                private int total_user;
                                private int total_post;

                                public String getIdentity() {
                                    return identity;
                                }

                                public void setIdentity(String identity) {
                                    this.identity = identity;
                                }

                                public String getName() {
                                    return name;
                                }

                                public void setName(String name) {
                                    this.name = name;
                                }

                                public int getTotal_user() {
                                    return total_user;
                                }

                                public void setTotal_user(int total_user) {
                                    this.total_user = total_user;
                                }

                                public int getTotal_post() {
                                    return total_post;
                                }

                                public void setTotal_post(int total_post) {
                                    this.total_post = total_post;
                                }
                            }
                        }
                    }
                }

                public static class AssetDetailBean {
                    private List<?> data;

                    public List<?> getData() {
                        return data;
                    }

                    public void setData(List<?> data) {
                        this.data = data;
                    }
                }

                public static class TagsBean {
                    private List<DataBeanXXXX> data;

                    public List<DataBeanXXXX> getData() {
                        return data;
                    }

                    public void setData(List<DataBeanXXXX> data) {
                        this.data = data;
                    }

                    public static class DataBeanXXXX {
                        /**
                         * identity : rpPDp
                         * name : firstpost
                         * tag_type : post
                         */

                        private String identity;
                        private String name;
                        private String tag_type;

                        public String getIdentity() {
                            return identity;
                        }

                        public void setIdentity(String identity) {
                            this.identity = identity;
                        }

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }

                        public String getTag_type() {
                            return tag_type;
                        }

                        public void setTag_type(String tag_type) {
                            this.tag_type = tag_type;
                        }
                    }
                }

                public static class UserTagBean {
                    private List<DataBeanXXXXX> data;

                    public List<DataBeanXXXXX> getData() {
                        return data;
                    }

                    public void setData(List<DataBeanXXXXX> data) {
                        this.data = data;
                    }

                    public static class DataBeanXXXXX {
                        /**
                         * identity : rpPDp
                         * name : null
                         * first_name : jack
                         * last_name : sparrow
                         * type : user
                         * profile_image : https://s3-us-west-2.amazonaws.com/talent-advocate/default-avatar.png
                         * auth_token : FRZiaBnfeo
                         * position : Developer
                         * department : HR
                         */

                        private String identity;
                        private Object name;
                        private String first_name;
                        private String last_name;
                        private String type;
                        private String profile_image;
                        private String auth_token;
                        private String position;
                        private String department;

                        public String getIdentity() {
                            return identity;
                        }

                        public void setIdentity(String identity) {
                            this.identity = identity;
                        }

                        public Object getName() {
                            return name;
                        }

                        public void setName(Object name) {
                            this.name = name;
                        }

                        public String getFirst_name() {
                            return first_name;
                        }

                        public void setFirst_name(String first_name) {
                            this.first_name = first_name;
                        }

                        public String getLast_name() {
                            return last_name;
                        }

                        public void setLast_name(String last_name) {
                            this.last_name = last_name;
                        }

                        public String getType() {
                            return type;
                        }

                        public void setType(String type) {
                            this.type = type;
                        }

                        public String getProfile_image() {
                            return profile_image;
                        }

                        public void setProfile_image(String profile_image) {
                            this.profile_image = profile_image;
                        }

                        public String getAuth_token() {
                            return auth_token;
                        }

                        public void setAuth_token(String auth_token) {
                            this.auth_token = auth_token;
                        }

                        public String getPosition() {
                            return position;
                        }

                        public void setPosition(String position) {
                            this.position = position;
                        }

                        public String getDepartment() {
                            return department;
                        }

                        public void setDepartment(String department) {
                            this.department = department;
                        }
                    }
                }
            }
        }
    }
}
