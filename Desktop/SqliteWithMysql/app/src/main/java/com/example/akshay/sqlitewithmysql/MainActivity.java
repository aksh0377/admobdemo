package com.example.akshay.sqlitewithmysql;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecylerViewAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX> list = new ArrayList<>();
    DbHelper db;
    Context context;
    Cursor cursor;
    byte[] img;
    String photo;

    boolean isConnected;
    SQLiteDatabase sqLiteDatabase;
    //1 means data is synced and 0 means data is not synced
    public static final int NAME_SYNCED_WITH_SERVER = 1;
    public static final int NAME_NOT_SYNCED_WITH_SERVER = 0;

    //Broadcast receiver to know the sync status
    private BroadcastReceiver broadcastReceiver;
    public static final String URL_SAVE_NAME ="http://api.visibly.io/api/v1/clients/post?api_secret=ch87QebMdJ&wall_type=all&auth_token=FRZiaBnfeo&page=1&hashtag=team&department_identity=rpPDp&limit=10&post_identity=Opjmp";


    //a broadcast to know weather the data is synced or not
    public static final String DATA_SAVED_BROADCAST = "net.simplifiedcoding.datasaved";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                .permitAll().build();
//        StrictMode.setThreadPolicy(policy);
        db = new DbHelper(this);

        recyclerView =(RecyclerView)findViewById(R.id.reyclerview);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
//calling the method to load all the stored names


        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(DATA_SAVED_BROADCAST));

         /*
         * ==========================
         * Sqlite Database operations
         * ==========================
         * */

         SQLiteDataBaseBuild();
         SQLiteTableBuild();
      //  DeletePreviousData();

         /*
         * ==========================
         * Check Network Conenction
         * ==========================
         * */



       ConnectivityManager cm =(ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
         isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

//         if(!isConnected)
//         {
//             ShowSQLiteDBdata();
//         }

        if(isConnected)

        {


            DeletePreviousData();
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading Data");
            progressDialog.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "http://api.staging.visibly.io/api/v1/clients/post?api_secret=ch87QebMdJ&wall_type=all&auth_token=FRZiaBnfeo&page=1&limit=10"
                    , null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        JSONArray jsonArray = response.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject dataa = jsonArray.getJSONObject(i);
                            JSONObject post = dataa.getJSONObject("post");
                            JSONObject post_data = post.getJSONObject("data");


                            data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX bean = new data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX (
                                    post_data.getString("identity"),
                                    post_data.getString("type"),
                                    post_data.getString("detail"),
                                    post_data.getString("approved"),
                                    post_data.getJSONObject("postOwner").getJSONObject("data").getString("avatar")
                            );



                              //      owner_data.getString("avatar");


//                        String demo = post_data.getString("detail");
//                        Log.e("detail",demo);
                            Toast.makeText(getApplicationContext(),post_data.getString("detail"),Toast.LENGTH_LONG).show();
                            Log.d("bean data photos", post_data.getJSONObject("postOwner").getJSONObject("data").getString("avatar"));
                            Log.d("bean data ",  post_data.getString("detail"));


                            list.add(bean);

                            /*
                             * ==================================
                             * inserting data into local Database
                             * ==================================
                             * */


                            String tempSubjectName = post_data.getString("detail");


                             /*
                             * ====================================================
                             * getting image from url and storing it into sqlite db
                             * =====================================================
                             * */


                            // String tempSubjectFullForm = jsonObject.getString("SubjectFullForm");
                            photo= post_data.getJSONObject("postOwner").getJSONObject("data").getString("avatar");
                            Log.v("photo",photo);




//                            try {
//                                URL url = new URL(post_data.getJSONObject("postOwner").getJSONObject("data").getString("avatar"));
//                                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                                img =getBytes(image);
//                            } catch(IOException e) {
//                                System.out.println(e);
//                            }






//                               String imageURL = URL[0];

//                            Bitmap bitmap;
//                            try {
//                                // Download Image from URL
//                                InputStream input = new java.net.URL(photo).openStream();
//
//
//                                Log.v("inputjbkblkbh", String.valueOf(input));
//
//                                bitmap = BitmapFactory.decodeStream(input);
//                                 img =getBytes(bitmap);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }

//

                            Toast.makeText(getApplicationContext(),"Done", Toast.LENGTH_SHORT).show();

//                            // content values to store data in sqlite db

                            ContentValues initialValues = new ContentValues();

                            initialValues.put("name", tempSubjectName);
                            initialValues.put("profile", photo);
                            sqLiteDatabase.insert(DbHelper.TABLE_NAME, null, initialValues);


                           // String SQLiteDataBaseQueryHolder = " INSERT INTO " +DbHelper.TABLE_NAME+ " (name,profile) VALUES ('"+tempSubjectName+"','"+photo+"');";

                            //sqLiteDatabase.execSQL(SQLiteDataBaseQueryHolder);
                        }

                        adapter = new RecylerViewAdapter(list, (Context) getApplicationContext());
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(), "Data fetched successfully", Toast.LENGTH_LONG);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    //  no_pending_leaves.setVisibility(View.VISIBLE);
                    // Toast.makeText(getActivity(), "sorry no data fetched", Toast.LENGTH_LONG);
                    Log.d("error", "error from server response");
                }
            });
            VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        }

      else if(!isConnected){


            ShowSQLiteDBdata() ;

        }


    }

    @Override
    protected void onResume() {
        //ShowSQLiteDBdata() ;
        super.onResume();
    }


    public void SQLiteDataBaseBuild(){

        sqLiteDatabase = openOrCreateDatabase(DbHelper.DB_NAME, Context.MODE_PRIVATE, null);

    }

    public void SQLiteTableBuild(){

        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+DbHelper.TABLE_NAME+"("+DbHelper.COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+DbHelper.COLUMN_NAME+" VARCHAR,"+DbHelper.COLUMN_PROFILE+" BLOB);");

    }

    public void DeletePreviousData(){

        sqLiteDatabase.execSQL("DELETE FROM "+DbHelper.TABLE_NAME+"");

    }


    private void ShowSQLiteDBdata() {

        sqLiteDatabase = db.getWritableDatabase();

        cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+DbHelper.TABLE_NAME+"", null);

       list.clear();
           if ( cursor.moveToFirst()) {
               do {


//populate table
                   String name=cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_NAME));

                 data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX bean=new data_pojo.DataBeanXXXXXXX.PostBean.DataBeanXXXXXX(cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_NAME)),cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_PROFILE)),cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_NAME)),cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_NAME)),cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_PROFILE)));
                   //ADD TO ARRAYLIS
                   Toast.makeText(getApplicationContext(),"jsut for test"+bean.getDetail() ,Toast.LENGTH_LONG).show();

                   list.add(bean);
//                       local_list.add(cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_NAME)));
                     //Toast.makeText(getApplicationContext(), " something found" +local_list, Toast.LENGTH_SHORT).show();


                       //Inserting Column Name into Array to Use at ListView Click Listener Method.
//               ListViewClickItemArray.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Name)));
//
//                Subject_NAME_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_1_Subject_Name)));
//
//                Subject_FullForm_Array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.Table_Column_2_SubjectFullForm)));


                   }
                   while (cursor.moveToNext()) ;
               adapter = new RecylerViewAdapter(list,getApplicationContext());
               //Toast.makeText(getApplicationContext(), " something found" +local_list, Toast.LENGTH_SHORT).show();

               recyclerView.setAdapter(adapter);
               adapter.notifyDataSetChanged();



           }
//        if(!(local_list.size()<1))
//        {
//            adapter = new RecylerViewAdapter(list,getApplicationContext());
//            Toast.makeText(getApplicationContext(), " something found" +local_list, Toast.LENGTH_SHORT).show();
//
//
//            recyclerView.setAdapter(adapter);
//            adapter.notifyDataSetChanged();
//        }


            //Toast.makeText(getApplicationContext()," context found"+local_list+cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_NAME)),Toast.LENGTH_SHORT).show();

//        }
//        else
//        {
//            adapter = new RecylerViewAdapter(local_list);
//            recyclerView.setAdapter(adapter);
//            adapter.notifyDataSetChanged();
//            Toast.makeText(getApplicationContext(),"no context found ",Toast.LENGTH_SHORT).show();
//        }

        cursor.close();
    }
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }




}